simple api using docker and symfony 4
========
## Installation

First, build the docker images

`docker-compose build`

Run the containers

`docker-compose up -d`

Now shell into the PHP container

`docker-compose exec php-fpm bash`

And install all the dependencies

`composer install`

To make sure our step is Ok, lets check our url: `http://localhost:8000/`

Check you menu available: `http://localhost:8000/api/lunch`
