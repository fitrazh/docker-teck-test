<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IngredientRepository")
 */
class Ingredient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $Title;

    /**
     * @ORM\Column(type="date")
     */
    private $BestBefore;

    /**
     * @ORM\Column(type="date")
     */
    private $UseBy;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->Title;
    }

    public function setTitle(string $Title): self
    {
        $this->Title = $Title;

        return $this;
    }

    public function getBestBefore(): ?\DateTimeInterface
    {
        return $this->BestBefore;
    }

    public function setBestBefore(\DateTimeInterface $BestBefore): self
    {
        $this->BestBefore = $BestBefore;

        return $this;
    }

    public function getUseBy(): ?\DateTimeInterface
    {
        return $this->UseBy;
    }

    public function setUseBy(\DateTimeInterface $UseBy): self
    {
        $this->UseBy = $UseBy;

        return $this;
    }
}
