<?php

namespace App\Controller\Api;


use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RecipesController{

  /**  * @Route("/lunch")  */  

  public function lunchAction()  {    
      
    $configDirectories = [__DIR__.'/Data'];

    $fileLocator = new FileLocator($configDirectories);
    
    $dataIngredientFiles = $fileLocator->locate('data-ingredient.json', null, false);
    $ingredientJson = file_get_contents($dataIngredientFiles[0]);
    $ingredients = json_decode($ingredientJson, true);
    

    $dataRecipesFiles = $fileLocator->locate('data-recipe.json', null, false);
    $recipesJson = file_get_contents($dataRecipesFiles[0]);
    $recipes = json_decode($recipesJson, true);
    
    echo "<pre>";

    $array = array(); //get list array ingredients in my fridge
    foreach ($ingredients['ingredients'] as $ingredient) {
      // print_r($ingredient);    
      // if ($ingredient['best-before']<=$ingredient['use-by'])
      // echo "exp";
      $ingredient['title'];

        $array[] = $ingredient['title'];    
    }

    $availableRecipes=array();
    foreach ($recipes['recipes'] as $ListRecipes){   
       
     if (sizeof(array_intersect($array, $ListRecipes['ingredients'])) == sizeof($ListRecipes['ingredients'])){
      $availableRecipes[]= $ListRecipes['title'];
     }

    }
   

      $response = new Response();    
      $response->setContent(json_encode($availableRecipes));    
    return $response;  
  }

}